import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from '../Modules/Home/HomeScreen';
import TodosScreen from '../Modules/Todos/TodosScreen';

const AppNavigator = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      title: 'Home',
    },
  },
  Todos: {
    screen: TodosScreen,
    navigationOptions: {
      title: 'ToDoS',
    },
  },
});

export default createAppContainer(AppNavigator);
