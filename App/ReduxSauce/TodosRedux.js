import { createActions, createReducer } from 'reduxsauce';

const { Types, Creators } = createActions({
  addTodo: ['payload'],
  toggleTodo: ['payload'],
  deleteTodo: ['payload'],
});

const INITIAL_STATE = [
  { id: 1, text: 'Sauce Item 1', done: false },
  { id: 2, text: 'Sauce Item 2', done: true },
  { id: 3, text: 'Sauce Item 3', done: true },
];

const addTodo = (state, action) => {
  return [...state, action.payload];
};

const toggleTodo = (state, action) => {
  const index = state.findIndex(x => x.id === action.payload.id);
  const newState = [...state];
  newState[index] = { ...newState[index], done: !newState[index].done };
  return newState;
};

const deleteTodo = (state, action) => {
  return state.filter(x => x.id !== action.payload.id);
};

export { Creators as Actions };

export default createReducer(INITIAL_STATE, {
  [Types.ADD_TODO]: addTodo,
  [Types.TOGGLE_TODO]: toggleTodo,
  [Types.DELETE_TODO]: deleteTodo,
});
