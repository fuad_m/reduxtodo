import { createStore } from 'redux';

import reducer from './TodosRedux';

const store = createStore(reducer);

export default store;
