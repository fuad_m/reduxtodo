import React from 'react';

import AppNavigation from '../Navigation/AppNavigation';

const App: () => React$Node = () => {
  return <AppNavigation />;
};

export default App;
