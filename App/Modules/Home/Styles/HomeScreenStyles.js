import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  button: {
    borderColor: 'lightgray',
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
    height: '30%',
  },
  buttonTitle: {
    fontSize: 22,
    fontWeight: 'bold',
  },
  buttonEmoji: {
    marginTop: 10,
    fontSize: 72,
  },
});

export default styles;
