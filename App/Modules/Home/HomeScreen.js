/**
 * @flow
 */

import React from 'react';
import { SafeAreaView, Text, TouchableOpacity } from 'react-native';
import { useNavigation } from 'react-navigation-hooks';
import styles from './Styles/HomeScreenStyles';

type ButtonType = {
  title: String,
  useSauce: Boolean,
};

const Button = ({ title, useSauce }: ButtonType) => {
  const navigation = useNavigation();

  const onClick = React.useCallback(() => {
    navigation.navigate('Todos', { useSauce });
  }, [useSauce, navigation]);

  return (
    <TouchableOpacity style={styles.button} onPress={onClick}>
      <Text style={styles.buttonTitle}>{title}</Text>
      <Text style={styles.buttonEmoji}>{useSauce ? '😀' : '😐'}</Text>
    </TouchableOpacity>
  );
};

const HomeScreen = () => {
  return (
    <SafeAreaView style={styles.container}>
      <Button title="Plain Redux" />
      <Button useSauce title="With ReduxSauce" />
    </SafeAreaView>
  );
};

export default HomeScreen;
