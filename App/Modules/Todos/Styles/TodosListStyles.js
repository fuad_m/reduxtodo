import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  list: {
    margin: 10,
    flex: 1,
  },
  item: {
    height: 40,
    flexDirection: 'row',
    alignItems: 'stretch',
  },
  itemText: {
    flexGrow: 1,
  },
  strike: {
    textDecorationLine: 'line-through',
  },
  deleteButton: {
    height: 40,
    width: 40,
  },
});

export default styles;
