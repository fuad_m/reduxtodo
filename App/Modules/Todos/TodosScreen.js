import React from 'react';
import { SafeAreaView, View, Text, TouchableOpacity } from 'react-native';
import { Provider, useSelector, useDispatch } from 'react-redux';
import { useNavigation } from 'react-navigation-hooks';

import styles from './Styles/TodosScreenStyles';

import plainReduxStore from '../../Redux/Store';
import { addTodo } from '../../Redux/actions';

import reduxSauceStore from '../../ReduxSauce/Store';
import { Actions } from '../../ReduxSauce/TodosRedux';
import List from './TodosList';

const AddButton = () => {
  const dispatch = useDispatch();
  const todos = useSelector(state => state);
  const navigation = useNavigation();
  const useSauce = navigation.getParam('useSauce');

  const id = (todos[todos.length - 1]?.id || 0) + 1;

  const todo = {
    id,
    text: `${useSauce ? 'Sauce' : 'Plain'} Item ${id}`,
    done: false,
  };

  const onAddClick = React.useCallback(() => {
    dispatch(useSauce ? Actions.addTodo(todo) : addTodo(todo));
  }, [dispatch, todo, useSauce]);

  return (
    <TouchableOpacity style={styles.floatingButton} onPress={onAddClick}>
      <Text style={styles.floatingButtonText}>➕</Text>
    </TouchableOpacity>
  );
};

const TodosScreen = () => {
  const navigation = useNavigation();
  const useSauce = navigation.getParam('useSauce');

  return (
    <Provider store={useSauce ? reduxSauceStore : plainReduxStore}>
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <List />
          <AddButton />
        </View>
      </SafeAreaView>
    </Provider>
  );
};

export default TodosScreen;
