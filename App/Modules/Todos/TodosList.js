import React from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigation } from 'react-navigation-hooks';

import styles from './Styles/TodosListStyles';

import { toggleTodo, deleteTodo } from '../../Redux/actions';
import { Actions } from '../../ReduxSauce/TodosRedux';

const TodoItem = ({ item: todo, index }) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const useSauce = navigation.getParam('useSauce');

  const onToggleClick = React.useCallback(() => {
    dispatch(useSauce ? Actions.toggleTodo(todo) : toggleTodo(todo));
  }, [dispatch, todo, useSauce]);

  const onDeleteClick = React.useCallback(() => {
    dispatch(useSauce ? Actions.deleteTodo(todo) : deleteTodo(todo));
  }, [dispatch, todo, useSauce]);

  return (
    <View style={styles.item}>
      <TouchableOpacity style={styles.itemText} onPress={onToggleClick}>
        <Text>
          <Text>{`${index + 1}. `}</Text>
          <Text style={todo.done && styles.strike}>{todo.text}</Text>
        </Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.deleteButton} onPress={onDeleteClick}>
        <Text>✘</Text>
      </TouchableOpacity>
    </View>
  );
};

const List = () => {
  const todos = useSelector(state => state);
  return (
    <FlatList
      style={styles.list}
      data={todos}
      keyExtractor={(item, index) => `${item}_${index}`}
      renderItem={item => <TodoItem {...item} />}
    />
  );
};

export default List;
