import { ADD_TODO, TOGGLE_TODO, DELETE_TODO } from '../types';

const INITIAL_STATE = [
  { id: 1, text: 'Plain Item 1', done: true, deleted: false },
  { id: 2, text: 'Plain Item 2', done: false, deleted: true },
  { id: 3, text: 'Plain Item 3', done: false, deleted: false },
];

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_TODO:
      return [...state, action.payload];

    case TOGGLE_TODO:
      const index = state.findIndex(x => x.id === action.payload.id);
      const newState = [...state];
      newState[index] = { ...newState[index], done: !newState[index].done };
      return newState;

    case DELETE_TODO:
      return state.filter(x => x.id !== action.payload.id);

    default:
      return state;
  }
};
